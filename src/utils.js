export const createVideoUrl = videoId => `http://www.youtube.com/watch?v=${videoId}`;

export const createEmbedVideoUrl = videoId => `//www.youtube.com/embed/${videoId}?autoplay=1`;

export const createVideoThumbnail = videoId => `//i.ytimg.com/vi/${videoId}/hqdefault.jpg`;
