import { createVideoUrl } from "@/utils";

export const getYoutubeData = async videoId => {
  const videoUrl = createVideoUrl(videoId);

  return fetch(`//noembed.com/embed?url=${encodeURIComponent(videoUrl)}`).then(res => res.json());
};

export const getYoutubeDataQueue = (() => {
  const TIMEOUT = 0;
  const queue = [];
  let tid;

  function startQueue() {
    clearTimeout(tid);

    tid = setTimeout(async () => {
      const { videoId, resolve, reject } = queue.shift() || {};
      if (!videoId) {
        return;
      }

      getYoutubeData(videoId)
        .then(resolve)
        .catch(reject)
        .then(startQueue);
    }, TIMEOUT);
  }

  return videoId =>
    new Promise((resolve, reject) => {
      queue.push({ videoId, resolve, reject });
      startQueue();
    });
})();
